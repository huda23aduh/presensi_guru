﻿Public Class Form_Jadwal_Mengajar

    'Mendeklarasikan variable
    Dim Proses As New Class_Connection
    Dim Proses_1 As New Class_Connection_1
    Dim TblKelas As DataTable
    Dim TblGuru As DataTable
    Dim SSQL As String

    Private Sub Form_Jadwal_Mengajar_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Proses_1.conecDB()
        Proses_1.initCMD()

        tampil_combo("guru")
        tampil_combo("kelas")
        tampil_combo("mata_pelajaran")


    End Sub

    Private Sub tampil_combo(ByVal tbl_name As String)

        Dim SQL As String

        If tbl_name = "guru" Then
            Try
                'Tampilkan User Akses Group Pada Combo Box
                SQL = "SELECT * FROM " + tbl_name

                With Proses_1.comDB
                    .CommandText = SQL
                    .ExecuteNonQuery()
                End With
                Proses_1.rdDB = Proses_1.comDB.ExecuteReader
                If Proses_1.rdDB.HasRows = True Then
                    combo_nip.Items.Clear()
                    While Proses_1.rdDB.Read()
                        combo_nip.Items.Add(Proses_1.rdDB("idguru").ToString() + "<===>" + Proses_1.rdDB("nip").ToString())
                    End While
                End If
                Proses_1.rdDB.Close()
            Catch ex As Exception
                MsgBox(ex.ToString)
            End Try
        End If

        If tbl_name = "kelas" Then
            Try
                'Tampilkan User Akses Group Pada Combo Box
                SQL = "SELECT * FROM " + tbl_name

                With Proses_1.comDB
                    .CommandText = SQL
                    .ExecuteNonQuery()
                End With
                Proses_1.rdDB = Proses_1.comDB.ExecuteReader
                If Proses_1.rdDB.HasRows = True Then
                    combo_kelas.Items.Clear()
                    While Proses_1.rdDB.Read()
                        Combo_kelas.Items.Add(Proses_1.rdDB("idkelas").ToString() + "<===>" + Proses_1.rdDB("nama_kelas").ToString())
                    End While
                End If
                Proses_1.rdDB.Close()
            Catch ex As Exception
                MsgBox(ex.ToString)
            End Try
        End If

        If tbl_name = "mata_pelajaran" Then
            Try
                'Tampilkan User Akses Group Pada Combo Box
                SQL = "SELECT * FROM " + tbl_name

                With Proses_1.comDB
                    .CommandText = SQL
                    .ExecuteNonQuery()
                End With
                Proses_1.rdDB = Proses_1.comDB.ExecuteReader
                If Proses_1.rdDB.HasRows = True Then
                    Combo_mapel.Items.Clear()
                    While Proses_1.rdDB.Read()
                        Combo_mapel.Items.Add(Proses_1.rdDB("idmata_pelajaran").ToString() + "<===>" + Proses_1.rdDB("nama_mata_pelajaran").ToString())
                    End While
                End If
                Proses_1.rdDB.Close()
            Catch ex As Exception
                MsgBox(ex.ToString)
            End Try
        End If



    End Sub


    Private Sub btn_home_Click(sender As Object, e As EventArgs) Handles btn_home.Click
        Form_Admin_Home.Show()
        Me.Hide()
    End Sub

    Private Sub btn_minimize_Click(sender As Object, e As EventArgs) Handles btn_minimize.Click
        Me.WindowState = FormWindowState.Minimized
    End Sub

    Private Sub btn_close_Click(sender As Object, e As EventArgs) Handles btn_close.Click
        Me.Close()
    End Sub

    Private Sub btn_lihat_jadwal_Click(sender As Object, e As EventArgs) Handles btn_lihat_jadwal.Click
        Dim idguru_value As String
        Dim idkelas_value As String
        Dim idmapel_value As String

        Dim strArr() As String

        idguru_value = combo_nip.Text
        idkelas_value = combo_kelas.Text
        idmapel_value = combo_mapel.Text

        strArr = idguru_value.Split(New String() {"<===>"}, StringSplitOptions.None)
        idguru_value = strArr(0)

        strArr = idkelas_value.Split(New String() {"<===>"}, StringSplitOptions.None)
        idkelas_value = strArr(0)

        strArr = idmapel_value.Split(New String() {"<===>"}, StringSplitOptions.None)
        idmapel_value = strArr(0)

        'MsgBox(idguru_value + "#" + idkelas_value + "#" + idmapel_value)

        'For count = 0 To strArr.Length - 1
        '    MsgBox(strArr(count))
        'Next

        Call TampilData(idguru_value, idkelas_value, idmapel_value)
    End Sub

    'Koding untuk menampilkan data ke DataGridView
    Private Sub TampilData(ByVal idguru_param As String, ByVal idkelas_param As String, ByVal idmapel_param As String)
        TblGuru = Proses.ExecuteQuery("SELECT waktu_mulai, waktu_selesai,durasi ,semester, status_kedatangan FROM jadwal_mengajar WHERE guru_idguru = " + idguru_param + " AND mata_pelajaran_idmata_pelajaran = " + idmapel_param + " AND kelas_idkelas = " + idkelas_param)
        DataGridView1.DataSource = TblGuru
        DataGridView1.ReadOnly = True
    End Sub
End Class