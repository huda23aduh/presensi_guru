﻿Public Class Form_Admin_Home
    Private Sub btn_to_mstr_guru_form_Click(sender As Object, e As EventArgs) Handles btn_to_mstr_guru_form.Click
        Form_Guru_Master_Data.Show()
        Me.Hide()
    End Sub

    Private Sub btn_minimize_Click(sender As Object, e As EventArgs) Handles btn_minimize.Click
        Me.WindowState = FormWindowState.Minimized
    End Sub

    Private Sub btn_close_Click(sender As Object, e As EventArgs) Handles btn_close.Click
        Me.Close()
    End Sub

    Private Sub btn_to_mstr_kelas_Click(sender As Object, e As EventArgs) Handles btn_to_mstr_kelas.Click
        Form_Kelas_Master_Data.Show()
        Me.Hide()
    End Sub

    Private Sub btn_to_mstr_mapel_Click(sender As Object, e As EventArgs) Handles btn_to_mstr_mapel.Click
        Form_MaPel_Master_Data.Show()
        Me.Hide()
    End Sub

    Private Sub btn_to_lihat_jadwal_Click(sender As Object, e As EventArgs) Handles btn_to_lihat_jadwal.Click
        Form_Jadwal_Mengajar.Show()
        Me.Hide()
    End Sub
End Class