﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form_Jadwal_Mengajar
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form_Jadwal_Mengajar))
        Me.Label1 = New System.Windows.Forms.Label()
        Me.combo_nip = New System.Windows.Forms.ComboBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.btn_home = New System.Windows.Forms.PictureBox()
        Me.btn_close = New System.Windows.Forms.PictureBox()
        Me.btn_minimize = New System.Windows.Forms.PictureBox()
        Me.combo_kelas = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.combo_mapel = New System.Windows.Forms.ComboBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.btn_lihat_jadwal = New System.Windows.Forms.Button()
        CType(Me.btn_home, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btn_close, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btn_minimize, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(297, 19)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(212, 25)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "JADWAL MENGAJAR"
        '
        'combo_nip
        '
        Me.combo_nip.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.combo_nip.FormattingEnabled = True
        Me.combo_nip.Location = New System.Drawing.Point(165, 56)
        Me.combo_nip.Name = "combo_nip"
        Me.combo_nip.Size = New System.Drawing.Size(190, 21)
        Me.combo_nip.TabIndex = 27
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(50, 59)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(30, 17)
        Me.Label8.TabIndex = 26
        Me.Label8.Text = "NIP"
        '
        'btn_home
        '
        Me.btn_home.Image = Global.Presensi_Guru.My.Resources.Resources.home_logo
        Me.btn_home.Location = New System.Drawing.Point(656, 4)
        Me.btn_home.Name = "btn_home"
        Me.btn_home.Size = New System.Drawing.Size(49, 48)
        Me.btn_home.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.btn_home.TabIndex = 59
        Me.btn_home.TabStop = False
        '
        'btn_close
        '
        Me.btn_close.Image = Global.Presensi_Guru.My.Resources.Resources.close_logo
        Me.btn_close.Location = New System.Drawing.Point(749, 7)
        Me.btn_close.Name = "btn_close"
        Me.btn_close.Size = New System.Drawing.Size(39, 37)
        Me.btn_close.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.btn_close.TabIndex = 58
        Me.btn_close.TabStop = False
        '
        'btn_minimize
        '
        Me.btn_minimize.Image = CType(resources.GetObject("btn_minimize.Image"), System.Drawing.Image)
        Me.btn_minimize.Location = New System.Drawing.Point(706, 11)
        Me.btn_minimize.Name = "btn_minimize"
        Me.btn_minimize.Size = New System.Drawing.Size(36, 33)
        Me.btn_minimize.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.btn_minimize.TabIndex = 57
        Me.btn_minimize.TabStop = False
        '
        'combo_kelas
        '
        Me.combo_kelas.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.combo_kelas.FormattingEnabled = True
        Me.combo_kelas.Location = New System.Drawing.Point(165, 83)
        Me.combo_kelas.Name = "combo_kelas"
        Me.combo_kelas.Size = New System.Drawing.Size(190, 21)
        Me.combo_kelas.TabIndex = 61
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(50, 86)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(52, 17)
        Me.Label2.TabIndex = 60
        Me.Label2.Text = "KELAS"
        '
        'combo_mapel
        '
        Me.combo_mapel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.combo_mapel.FormattingEnabled = True
        Me.combo_mapel.Location = New System.Drawing.Point(165, 110)
        Me.combo_mapel.Name = "combo_mapel"
        Me.combo_mapel.Size = New System.Drawing.Size(190, 21)
        Me.combo_mapel.TabIndex = 63
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(50, 113)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(54, 17)
        Me.Label3.TabIndex = 62
        Me.Label3.Text = "MAPEL"
        '
        'DataGridView1
        '
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Location = New System.Drawing.Point(12, 199)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.Size = New System.Drawing.Size(776, 289)
        Me.DataGridView1.TabIndex = 64
        '
        'btn_lihat_jadwal
        '
        Me.btn_lihat_jadwal.Location = New System.Drawing.Point(53, 146)
        Me.btn_lihat_jadwal.Name = "btn_lihat_jadwal"
        Me.btn_lihat_jadwal.Size = New System.Drawing.Size(302, 23)
        Me.btn_lihat_jadwal.TabIndex = 65
        Me.btn_lihat_jadwal.Text = "LIHAT JADWAL "
        Me.btn_lihat_jadwal.UseVisualStyleBackColor = True
        '
        'Form_Jadwal_Mengajar
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Goldenrod
        Me.ClientSize = New System.Drawing.Size(800, 500)
        Me.Controls.Add(Me.btn_lihat_jadwal)
        Me.Controls.Add(Me.DataGridView1)
        Me.Controls.Add(Me.combo_mapel)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.combo_kelas)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.btn_home)
        Me.Controls.Add(Me.btn_close)
        Me.Controls.Add(Me.btn_minimize)
        Me.Controls.Add(Me.combo_nip)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "Form_Jadwal_Mengajar"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Form_Jadwal_Mengajar"
        CType(Me.btn_home, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btn_close, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btn_minimize, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label1 As Label
    Friend WithEvents combo_nip As ComboBox
    Friend WithEvents Label8 As Label
    Friend WithEvents btn_home As PictureBox
    Friend WithEvents btn_close As PictureBox
    Friend WithEvents btn_minimize As PictureBox
    Friend WithEvents combo_kelas As ComboBox
    Friend WithEvents Label2 As Label
    Friend WithEvents combo_mapel As ComboBox
    Friend WithEvents Label3 As Label
    Friend WithEvents DataGridView1 As DataGridView
    Friend WithEvents btn_lihat_jadwal As Button
End Class
