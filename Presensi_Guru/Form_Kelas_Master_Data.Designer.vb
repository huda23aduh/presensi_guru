﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form_Kelas_Master_Data
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form_Kelas_Master_Data))
        Me.label_idkelas = New System.Windows.Forms.Label()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.btn_delete = New System.Windows.Forms.Button()
        Me.btn_edit = New System.Windows.Forms.Button()
        Me.btn_add = New System.Windows.Forms.Button()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txt_nama_kelas = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txt_kode_kelas = New System.Windows.Forms.TextBox()
        Me.btn_home = New System.Windows.Forms.PictureBox()
        Me.btn_close = New System.Windows.Forms.PictureBox()
        Me.btn_minimize = New System.Windows.Forms.PictureBox()
        Me.Label1 = New System.Windows.Forms.Label()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btn_home, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btn_close, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btn_minimize, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'label_idkelas
        '
        Me.label_idkelas.AutoSize = True
        Me.label_idkelas.Location = New System.Drawing.Point(351, 68)
        Me.label_idkelas.Name = "label_idkelas"
        Me.label_idkelas.Size = New System.Drawing.Size(22, 13)
        Me.label_idkelas.TabIndex = 65
        Me.label_idkelas.Text = "- - -"
        Me.label_idkelas.Visible = False
        '
        'DataGridView1
        '
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Location = New System.Drawing.Point(25, 163)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.Size = New System.Drawing.Size(376, 327)
        Me.DataGridView1.TabIndex = 64
        '
        'btn_delete
        '
        Me.btn_delete.Location = New System.Drawing.Point(187, 127)
        Me.btn_delete.Name = "btn_delete"
        Me.btn_delete.Size = New System.Drawing.Size(75, 23)
        Me.btn_delete.TabIndex = 63
        Me.btn_delete.Text = "delete"
        Me.btn_delete.UseVisualStyleBackColor = True
        '
        'btn_edit
        '
        Me.btn_edit.Location = New System.Drawing.Point(106, 127)
        Me.btn_edit.Name = "btn_edit"
        Me.btn_edit.Size = New System.Drawing.Size(75, 23)
        Me.btn_edit.TabIndex = 62
        Me.btn_edit.Text = "edit"
        Me.btn_edit.UseVisualStyleBackColor = True
        '
        'btn_add
        '
        Me.btn_add.Location = New System.Drawing.Point(25, 127)
        Me.btn_add.Name = "btn_add"
        Me.btn_add.Size = New System.Drawing.Size(75, 23)
        Me.btn_add.TabIndex = 61
        Me.btn_add.Text = "add"
        Me.btn_add.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(24, 90)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(95, 17)
        Me.Label3.TabIndex = 60
        Me.Label3.Text = "NAMA KELAS"
        '
        'txt_nama_kelas
        '
        Me.txt_nama_kelas.Location = New System.Drawing.Point(207, 88)
        Me.txt_nama_kelas.Name = "txt_nama_kelas"
        Me.txt_nama_kelas.Size = New System.Drawing.Size(100, 20)
        Me.txt_nama_kelas.TabIndex = 59
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(24, 64)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(95, 17)
        Me.Label2.TabIndex = 58
        Me.Label2.Text = "KODE KELAS"
        '
        'txt_kode_kelas
        '
        Me.txt_kode_kelas.Location = New System.Drawing.Point(207, 62)
        Me.txt_kode_kelas.Name = "txt_kode_kelas"
        Me.txt_kode_kelas.Size = New System.Drawing.Size(100, 20)
        Me.txt_kode_kelas.TabIndex = 57
        '
        'btn_home
        '
        Me.btn_home.Image = Global.Presensi_Guru.My.Resources.Resources.home_logo
        Me.btn_home.Location = New System.Drawing.Point(645, 11)
        Me.btn_home.Name = "btn_home"
        Me.btn_home.Size = New System.Drawing.Size(49, 48)
        Me.btn_home.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.btn_home.TabIndex = 56
        Me.btn_home.TabStop = False
        '
        'btn_close
        '
        Me.btn_close.Image = Global.Presensi_Guru.My.Resources.Resources.close_logo
        Me.btn_close.Location = New System.Drawing.Point(738, 14)
        Me.btn_close.Name = "btn_close"
        Me.btn_close.Size = New System.Drawing.Size(39, 37)
        Me.btn_close.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.btn_close.TabIndex = 55
        Me.btn_close.TabStop = False
        '
        'btn_minimize
        '
        Me.btn_minimize.Image = CType(resources.GetObject("btn_minimize.Image"), System.Drawing.Image)
        Me.btn_minimize.Location = New System.Drawing.Point(695, 18)
        Me.btn_minimize.Name = "btn_minimize"
        Me.btn_minimize.Size = New System.Drawing.Size(36, 33)
        Me.btn_minimize.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.btn_minimize.TabIndex = 54
        Me.btn_minimize.TabStop = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(238, 18)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(227, 25)
        Me.Label1.TabIndex = 53
        Me.Label1.Text = "MASTER DATA KELAS"
        '
        'Form_Kelas_Master_Data
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Goldenrod
        Me.ClientSize = New System.Drawing.Size(800, 500)
        Me.Controls.Add(Me.label_idkelas)
        Me.Controls.Add(Me.DataGridView1)
        Me.Controls.Add(Me.btn_delete)
        Me.Controls.Add(Me.btn_edit)
        Me.Controls.Add(Me.btn_add)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txt_nama_kelas)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txt_kode_kelas)
        Me.Controls.Add(Me.btn_home)
        Me.Controls.Add(Me.btn_close)
        Me.Controls.Add(Me.btn_minimize)
        Me.Controls.Add(Me.Label1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "Form_Kelas_Master_Data"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Form_Kelas_Master_Data"
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btn_home, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btn_close, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btn_minimize, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents label_idkelas As Label
    Friend WithEvents DataGridView1 As DataGridView
    Friend WithEvents btn_delete As Button
    Friend WithEvents btn_edit As Button
    Friend WithEvents btn_add As Button
    Friend WithEvents Label3 As Label
    Friend WithEvents txt_nama_kelas As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents txt_kode_kelas As TextBox
    Friend WithEvents btn_home As PictureBox
    Friend WithEvents btn_close As PictureBox
    Friend WithEvents btn_minimize As PictureBox
    Friend WithEvents Label1 As Label
End Class
