﻿Imports MySql.Data
Imports MySql.Data.MySqlClient
Public Class Form_Guru_Master_Data

    'Mendeklarasikan variable
    Dim Proses As New Class_Connection
    Dim TblGuru As DataTable
    Dim SSQL As String

    'Koding untuk menampilkan data ke DataGridView
    Private Sub TampilData()
        TblGuru = Proses.ExecuteQuery("SELECT nip, nuptk, nama, telepon, alamat, jabatan, jenis_kelamin, tempat_lahir, tgl_lahir FROM guru")
        DataGridView1.DataSource = TblGuru
        DataGridView1.ReadOnly = True
    End Sub

    Private Sub combo_jenis_kelamin_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        combo_jenis_kelamin.Text = "PRIA"
    End Sub

    Private Sub Form_Teacher_Master_Data_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call TampilData()

        Dim comboSource As New Dictionary(Of String, String)()
        comboSource.Add("1", "PRIA")
        comboSource.Add("2", "WANITA")
        combo_jenis_kelamin.DataSource = New BindingSource(comboSource, Nothing)
        combo_jenis_kelamin.DisplayMember = "Value"
        combo_jenis_kelamin.ValueMember = "Key"

    End Sub

    Private Sub DataGridView1_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellClick
        txt_nip.ReadOnly = True
        txt_nuptk.ReadOnly = True

        Dim i, j As Integer
        i = DataGridView1.CurrentRow.Index
        txt_nip.Text = DataGridView1.Item(0, i).Value
        txt_nuptk.Text = DataGridView1.Item(1, i).Value
        txt_nama.Text = DataGridView1.Item(2, i).Value
        txt_telp.Text = DataGridView1.Item(3, i).Value
        txt_alamat.Text = DataGridView1.Item(4, i).Value
        txt_jabatan.Text = DataGridView1.Item(5, i).Value
        combo_jenis_kelamin.Text = DataGridView1.Item(6, i).Value
        txt_tempat_lahir.Text = DataGridView1.Item(7, i).Value
        datepicker_tgl_lahir.Value = DataGridView1.Item(8, i).Value
    End Sub



    Private Sub btn_add_Click(sender As Object, e As EventArgs) Handles btn_add.Click

        Dim conn As MySqlConnection

        conn = Proses.OpenTheConn()

        Dim sql As String = "insert into guru (nip, nuptk, nama, telepon, alamat, jabatan, jenis_kelamin, tempat_lahir, tgl_lahir) 
            values (@nip, @nuptk, @nama, @telepon, @alamat, @jabatan, @jenis_kelamin, @tempat_lahir, @tgl_lahir)"

        Dim cmd = New MySqlCommand(sql, conn)

        cmd.Parameters.AddWithValue("@nip", txt_nip.Text)
        cmd.Parameters.AddWithValue("@nuptk", txt_nuptk.Text)
        cmd.Parameters.AddWithValue("@nama", txt_nama.Text)
        cmd.Parameters.AddWithValue("@telepon", txt_telp.Text)
        cmd.Parameters.AddWithValue("@alamat", txt_alamat.Text)
        cmd.Parameters.AddWithValue("@jabatan", txt_jabatan.Text)
        cmd.Parameters.AddWithValue("@jenis_kelamin", combo_jenis_kelamin.SelectedValue.ToString())
        cmd.Parameters.AddWithValue("@tempat_lahir", txt_tempat_lahir.Text)
        cmd.Parameters.AddWithValue("@tgl_lahir", datepicker_tgl_lahir.Value.Year.ToString() + "-" + datepicker_tgl_lahir.Value.Month.ToString() + "-" + datepicker_tgl_lahir.Value.Day.ToString())

        Try
            cmd.ExecuteNonQuery()
            conn.Close()
            Call TampilData()
            Call Kosongkan()

        Catch ex As Exception
            MessageBox.Show("Proses Penyimpanan Gagal !, Karena " & ex.Message)
            conn.Close()
        End Try

    End Sub

    'Koding untuk mengosongkan Textbox
    Private Sub Kosongkan()
        txt_nip.Text = ""
        txt_nuptk.Text = ""
        txt_nama.Text = ""
        txt_telp.Text = ""
        txt_alamat.Text = ""
        txt_jabatan.Text = ""
        combo_jenis_kelamin.Text = ""
        txt_tempat_lahir.Text = ""
        datepicker_tgl_lahir.ResetText()
        'txtNim.Focus()
    End Sub

    Private Sub btn_edit_Click(sender As Object, e As EventArgs) Handles btn_edit.Click
        Dim conn As MySqlConnection

        conn = Proses.OpenTheConn()

        Dim sql As String = "update guru SET nama =  @nama, telepon = @telepon, alamat = @alamat, jabatan = @jabatan, jenis_kelamin = @jenis_kelamin, tempat_lahir = @tempat_lahir, tgl_lahir = @tgl_lahir  WHERE nip = @nip"

        Dim cmd = New MySqlCommand(sql, conn)

        cmd.Parameters.AddWithValue("@nip", txt_nip.Text)
        'cmd.Parameters.AddWithValue("@nuptk", txt_nuptk.Text)
        cmd.Parameters.AddWithValue("@nama", txt_nama.Text)
        cmd.Parameters.AddWithValue("@telepon", txt_telp.Text)
        cmd.Parameters.AddWithValue("@alamat", txt_alamat.Text)
        cmd.Parameters.AddWithValue("@jabatan", txt_jabatan.Text)
        cmd.Parameters.AddWithValue("@jenis_kelamin", combo_jenis_kelamin.SelectedValue.ToString())
        cmd.Parameters.AddWithValue("@tempat_lahir", txt_tempat_lahir.Text)
        cmd.Parameters.AddWithValue("@tgl_lahir", datepicker_tgl_lahir.Value.Year.ToString() + "-" + datepicker_tgl_lahir.Value.Month.ToString() + "-" + datepicker_tgl_lahir.Value.Day.ToString())


        Try
            cmd.ExecuteNonQuery()
            conn.Close()
            MsgBox("Data Berhasil diUpdate")
            Call TampilData()
            Call Kosongkan()

        Catch ex As Exception
            MessageBox.Show("Proses edit Gagal !, Karena " & ex.Message)
            conn.Close()
        End Try


    End Sub

    Private Sub btn_delete_Click(sender As Object, e As EventArgs) Handles btn_delete.Click
        Dim conn As MySqlConnection

        conn = Proses.OpenTheConn()

        Dim sql As String = "DELETE From guru Where nip = @nip "

        Dim cmd = New MySqlCommand(sql, conn)

        cmd.Parameters.AddWithValue("@nip", txt_nip.Text)


        Try
            cmd.ExecuteNonQuery()
            conn.Close()
            MsgBox("Data Berhasil dihapus")
            Call TampilData()
            Call Kosongkan()

        Catch ex As Exception
            MessageBox.Show("Proses hapus Gagal !, Karena " & ex.Message)
            conn.Close()
        End Try


    End Sub

    Private Sub btn_home_Click(sender As Object, e As EventArgs) Handles btn_home.Click
        Form_Admin_Home.Show()
        Me.Hide()
    End Sub

    Private Sub btn_minimize_Click(sender As Object, e As EventArgs) Handles btn_minimize.Click
        Me.WindowState = FormWindowState.Minimized
    End Sub

    Private Sub btn_close_Click(sender As Object, e As EventArgs) Handles btn_close.Click
        Me.Close()
    End Sub
End Class