﻿Imports MySql.Data.MySqlClient

Public Class Form_Guru_Login

    'Mendeklarasikan variable
    Dim Proses As New Class_Connection
    Dim Sql As String

    Private Sub btn_home_Click(sender As Object, e As EventArgs) Handles btn_home.Click
        Form_Home.Show()
        Me.Hide()
    End Sub

    Private Sub btn_minimize_Click(sender As Object, e As EventArgs) Handles btn_minimize.Click
        Me.WindowState = FormWindowState.Minimized
    End Sub

    Private Sub btn_close_Click(sender As Object, e As EventArgs) Handles btn_close.Click
        Me.Close()
    End Sub

    Private Sub btn_process_login_Click(sender As Object, e As EventArgs) Handles btn_process_login.Click
        Dim conn As MySqlConnection
        conn = Proses.OpenTheConn()


        Dim myAdapter As New MySqlDataAdapter

        Dim sqlquery = "SELECT * FROM user WHERE username = '" + TextBox1.Text + "' AND password= '" + TextBox2.Text + "'"
        Dim myCommand As New MySqlCommand
        myCommand.Connection = conn
        myCommand.CommandText = sqlquery

        myAdapter.SelectCommand = myCommand
        Dim myData As MySqlDataReader
        myData = myCommand.ExecuteReader()

        If myData.HasRows = 0 Then
            MsgBox("Username atau Password ada yang salah !", MsgBoxStyle.Exclamation, "Error Login")
        Else
            MsgBox("Login Berhasil, Selamat Datang " & TextBox1.Text & " ! ", MsgBoxStyle.Information, "Successfull Login")
            Form_Guru_Home.Show()
            Me.Hide()
        End If
    End Sub
End Class