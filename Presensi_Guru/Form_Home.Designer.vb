﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Form_Home
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btn_to_login_page = New System.Windows.Forms.Button()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.btn_to_admin_login_page = New System.Windows.Forms.Button()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 30.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(222, 78)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(385, 46)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "SELAMAT DATANG"
        '
        'btn_to_login_page
        '
        Me.btn_to_login_page.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_to_login_page.Location = New System.Drawing.Point(207, 335)
        Me.btn_to_login_page.Name = "btn_to_login_page"
        Me.btn_to_login_page.Size = New System.Drawing.Size(188, 61)
        Me.btn_to_login_page.TabIndex = 1
        Me.btn_to_login_page.Text = "MASUK SEBAGAI GURU"
        Me.btn_to_login_page.UseVisualStyleBackColor = True
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = Global.Presensi_Guru.My.Resources.Resources.logo
        Me.PictureBox1.Location = New System.Drawing.Point(338, 153)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(150, 136)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 2
        Me.PictureBox1.TabStop = False
        '
        'btn_to_admin_login_page
        '
        Me.btn_to_admin_login_page.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_to_admin_login_page.Location = New System.Drawing.Point(413, 335)
        Me.btn_to_admin_login_page.Name = "btn_to_admin_login_page"
        Me.btn_to_admin_login_page.Size = New System.Drawing.Size(214, 61)
        Me.btn_to_admin_login_page.TabIndex = 3
        Me.btn_to_admin_login_page.Text = "MASUK SEBAGAI ADMIN"
        Me.btn_to_admin_login_page.UseVisualStyleBackColor = True
        '
        'Form_Home
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Goldenrod
        Me.ClientSize = New System.Drawing.Size(800, 500)
        Me.Controls.Add(Me.btn_to_admin_login_page)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.btn_to_login_page)
        Me.Controls.Add(Me.Label1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "Form_Home"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Form1"
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label1 As Label
    Friend WithEvents btn_to_login_page As Button
    Friend WithEvents PictureBox1 As PictureBox
    Friend WithEvents btn_to_admin_login_page As Button
End Class
