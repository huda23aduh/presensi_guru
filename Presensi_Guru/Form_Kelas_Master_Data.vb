﻿Imports MySql.Data
Imports MySql.Data.MySqlClient

Public Class Form_Kelas_Master_Data

    Private Sub Form_Kelas_Master_Data_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call TampilData()

    End Sub

    'Mendeklarasikan variable
    Dim Proses As New Class_Connection
    Dim TblKelas As DataTable
    Dim SSQL As String

    'Koding untuk menampilkan data ke DataGridView
    Private Sub TampilData()
        TblKelas = Proses.ExecuteQuery("SELECT * FROM kelas")
        DataGridView1.DataSource = TblKelas
        DataGridView1.ReadOnly = True
    End Sub

    'Koding untuk mengosongkan Textbox
    Private Sub Kosongkan()
        txt_nama_kelas.Text = ""
        txt_kode_kelas.Text = ""
    End Sub


    Private Sub btn_home_Click(sender As Object, e As EventArgs) Handles btn_home.Click
        Form_Admin_Home.Show()
        Me.Hide()
    End Sub

    Private Sub btn_minimize_Click(sender As Object, e As EventArgs) Handles btn_minimize.Click
        Me.WindowState = FormWindowState.Minimized
    End Sub

    Private Sub btn_close_Click(sender As Object, e As EventArgs) Handles btn_close.Click
        Me.Close()
    End Sub

    Private Sub DataGridView1_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellClick

        Dim i, j As Integer
        i = DataGridView1.CurrentRow.Index
        label_idkelas.Text = DataGridView1.Item(0, i).Value
        txt_nama_kelas.Text = DataGridView1.Item(1, i).Value
        txt_kode_kelas.Text = DataGridView1.Item(2, i).Value
    End Sub

    Private Sub btn_add_Click(sender As Object, e As EventArgs) Handles btn_add.Click
        Dim conn As MySqlConnection

        conn = Proses.OpenTheConn()

        Dim sql As String = "insert into kelas (nama_kelas, kode_kelas)  values (@nama_kelas, @kode_kelas)"

        Dim cmd = New MySqlCommand(sql, conn)

        cmd.Parameters.AddWithValue("@nama_kelas", txt_nama_kelas.Text)
        cmd.Parameters.AddWithValue("@kode_kelas", txt_kode_kelas.Text)

        Try
            cmd.ExecuteNonQuery()
            conn.Close()
            Call TampilData()
            Call Kosongkan()

        Catch ex As Exception
            MessageBox.Show("Proses Penyimpanan Gagal !, Karena " & ex.Message)
            conn.Close()
        End Try
    End Sub

    Private Sub btn_edit_Click(sender As Object, e As EventArgs) Handles btn_edit.Click
        Dim conn As MySqlConnection

        conn = Proses.OpenTheConn()

        Dim sql As String = "update kelas SET nama_kelas =  @nama_kelas, kode_kelas = @kode_kelas WHERE idkelas = @idkelas"

        Dim cmd = New MySqlCommand(sql, conn)

        cmd.Parameters.AddWithValue("@idkelas", label_idkelas.Text)
        cmd.Parameters.AddWithValue("@kode_kelas", txt_kode_kelas.Text)
        cmd.Parameters.AddWithValue("@nama_kelas", txt_nama_kelas.Text)


        Try
            cmd.ExecuteNonQuery()
            conn.Close()
            MsgBox("Data Berhasil diUpdate")
            Call TampilData()
            Call Kosongkan()

        Catch ex As Exception
            MessageBox.Show("Proses edit Gagal !, Karena " & ex.Message)
            conn.Close()
        End Try
    End Sub

    Private Sub btn_delete_Click(sender As Object, e As EventArgs) Handles btn_delete.Click
        Dim conn As MySqlConnection

        conn = Proses.OpenTheConn()

        Dim sql As String = "DELETE From kelas Where idkelas = @idkelas "

        Dim cmd = New MySqlCommand(sql, conn)

        cmd.Parameters.AddWithValue("@idkelas", label_idkelas.Text)


        Try
            cmd.ExecuteNonQuery()
            conn.Close()
            MsgBox("Data Berhasil dihapus")
            Call TampilData()
            Call Kosongkan()

        Catch ex As Exception
            MessageBox.Show("Proses hapus Gagal !, Karena " & ex.Message)
            conn.Close()
        End Try
    End Sub
End Class