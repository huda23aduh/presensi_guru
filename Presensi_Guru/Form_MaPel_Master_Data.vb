﻿Imports MySql.Data
Imports MySql.Data.MySqlClient

Public Class Form_MaPel_Master_Data

    Private Sub Form_MaPel_Master_Data_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call TampilData()

    End Sub

    'Mendeklarasikan variable
    Dim Proses As New Class_Connection
    Dim TblKelas As DataTable
    Dim SSQL As String

    'Koding untuk menampilkan data ke DataGridView
    Private Sub TampilData()
        TblKelas = Proses.ExecuteQuery("SELECT * FROM mata_pelajaran")
        DataGridView1.DataSource = TblKelas
        DataGridView1.ReadOnly = True
    End Sub

    'Koding untuk mengosongkan Textbox
    Private Sub Kosongkan()
        txt_nama_mapel.Text = ""
        txt_kode_mapel.Text = ""
    End Sub

    Private Sub btn_home_Click(sender As Object, e As EventArgs) Handles btn_home.Click
        Form_Admin_Home.Show()
        Me.Hide()
    End Sub

    Private Sub btn_minimize_Click(sender As Object, e As EventArgs) Handles btn_minimize.Click
        Me.WindowState = FormWindowState.Minimized
    End Sub

    Private Sub btn_close_Click(sender As Object, e As EventArgs) Handles btn_close.Click
        Me.Close()
    End Sub

    Private Sub DataGridView1_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellClick

        Dim i, j As Integer
        i = DataGridView1.CurrentRow.Index
        label_idmata_pelajaran.Text = DataGridView1.Item(0, i).Value
        txt_nama_mapel.Text = DataGridView1.Item(1, i).Value
        txt_kode_mapel.Text = DataGridView1.Item(2, i).Value
    End Sub

    Private Sub btn_add_Click(sender As Object, e As EventArgs) Handles btn_add.Click
        Dim conn As MySqlConnection

        conn = Proses.OpenTheConn()

        Dim sql As String = "insert into mata_pelajaran (nama_mata_pelajaran, kode_mata_pelajaran)  values (@nama_mata_pelajaran, @kode_mata_pelajaran)"

        Dim cmd = New MySqlCommand(sql, conn)

        cmd.Parameters.AddWithValue("@nama_mata_pelajaran", txt_nama_mapel.Text)
        cmd.Parameters.AddWithValue("@kode_mata_pelajaran", txt_kode_mapel.Text)

        Try
            cmd.ExecuteNonQuery()
            conn.Close()
            Call TampilData()
            Call Kosongkan()

        Catch ex As Exception
            MessageBox.Show("Proses Penyimpanan Gagal !, Karena " & ex.Message)
            conn.Close()
        End Try
    End Sub

    Private Sub btn_edit_Click(sender As Object, e As EventArgs) Handles btn_edit.Click
        Dim conn As MySqlConnection

        conn = Proses.OpenTheConn()

        Dim sql As String = "update mata_pelajaran SET nama_mata_pelajaran =  @nama_mata_pelajaran, kode_mata_pelajaran = @kode_mata_pelajaran WHERE idmata_pelajaran = @idmata_pelajaran"

        Dim cmd = New MySqlCommand(sql, conn)

        cmd.Parameters.AddWithValue("@idmata_pelajaran", label_idmata_pelajaran.Text)
        cmd.Parameters.AddWithValue("@kode_mata_pelajaran", txt_kode_mapel.Text)
        cmd.Parameters.AddWithValue("@nama_mata_pelajaran", txt_nama_mapel.Text)


        Try
            cmd.ExecuteNonQuery()
            conn.Close()
            MsgBox("Data Berhasil diUpdate")
            Call TampilData()
            Call Kosongkan()

        Catch ex As Exception
            MessageBox.Show("Proses edit Gagal !, Karena " & ex.Message)
            conn.Close()
        End Try
    End Sub

    Private Sub btn_delete_Click(sender As Object, e As EventArgs) Handles btn_delete.Click
        Dim conn As MySqlConnection

        conn = Proses.OpenTheConn()

        Dim sql As String = "DELETE From mata_pelajaran Where idmata_pelajaran = @idmata_pelajaran "

        Dim cmd = New MySqlCommand(sql, conn)

        cmd.Parameters.AddWithValue("@idmata_pelajaran", label_idmata_pelajaran.Text)


        Try
            cmd.ExecuteNonQuery()
            conn.Close()
            MsgBox("Data Berhasil dihapus")
            Call TampilData()
            Call Kosongkan()

        Catch ex As Exception
            MessageBox.Show("Proses hapus Gagal !, Karena " & ex.Message)
            conn.Close()
        End Try
    End Sub
End Class