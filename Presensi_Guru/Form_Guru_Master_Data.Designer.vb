﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Form_Guru_Master_Data
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form_Guru_Master_Data))
        Me.Label1 = New System.Windows.Forms.Label()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.txt_nip = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txt_nuptk = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txt_nama = New System.Windows.Forms.TextBox()
        Me.btn_add = New System.Windows.Forms.Button()
        Me.btn_edit = New System.Windows.Forms.Button()
        Me.btn_delete = New System.Windows.Forms.Button()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txt_telp = New System.Windows.Forms.TextBox()
        Me.txt_alamat = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txt_jabatan = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txt_tempat_lahir = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.combo_jenis_kelamin = New System.Windows.Forms.ComboBox()
        Me.datepicker_tgl_lahir = New System.Windows.Forms.DateTimePicker()
        Me.btn_close = New System.Windows.Forms.PictureBox()
        Me.btn_minimize = New System.Windows.Forms.PictureBox()
        Me.btn_home = New System.Windows.Forms.PictureBox()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btn_close, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btn_minimize, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btn_home, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(297, 19)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(217, 25)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "MASTER DATA GURU"
        '
        'DataGridView1
        '
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Location = New System.Drawing.Point(12, 270)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.Size = New System.Drawing.Size(760, 179)
        Me.DataGridView1.TabIndex = 3
        '
        'txt_nip
        '
        Me.txt_nip.Location = New System.Drawing.Point(180, 69)
        Me.txt_nip.Name = "txt_nip"
        Me.txt_nip.Size = New System.Drawing.Size(100, 20)
        Me.txt_nip.TabIndex = 4
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(65, 69)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(30, 17)
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "NIP"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(64, 95)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(55, 17)
        Me.Label3.TabIndex = 7
        Me.Label3.Text = "NUPTK"
        '
        'txt_nuptk
        '
        Me.txt_nuptk.Location = New System.Drawing.Point(180, 95)
        Me.txt_nuptk.Name = "txt_nuptk"
        Me.txt_nuptk.Size = New System.Drawing.Size(100, 20)
        Me.txt_nuptk.TabIndex = 6
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(65, 123)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(47, 17)
        Me.Label4.TabIndex = 9
        Me.Label4.Text = "NAMA"
        '
        'txt_nama
        '
        Me.txt_nama.Location = New System.Drawing.Point(180, 123)
        Me.txt_nama.Name = "txt_nama"
        Me.txt_nama.Size = New System.Drawing.Size(100, 20)
        Me.txt_nama.TabIndex = 8
        '
        'btn_add
        '
        Me.btn_add.Location = New System.Drawing.Point(462, 242)
        Me.btn_add.Name = "btn_add"
        Me.btn_add.Size = New System.Drawing.Size(75, 23)
        Me.btn_add.TabIndex = 10
        Me.btn_add.Text = "add"
        Me.btn_add.UseVisualStyleBackColor = True
        '
        'btn_edit
        '
        Me.btn_edit.Location = New System.Drawing.Point(543, 242)
        Me.btn_edit.Name = "btn_edit"
        Me.btn_edit.Size = New System.Drawing.Size(75, 23)
        Me.btn_edit.TabIndex = 11
        Me.btn_edit.Text = "edit"
        Me.btn_edit.UseVisualStyleBackColor = True
        '
        'btn_delete
        '
        Me.btn_delete.Location = New System.Drawing.Point(624, 242)
        Me.btn_delete.Name = "btn_delete"
        Me.btn_delete.Size = New System.Drawing.Size(75, 23)
        Me.btn_delete.TabIndex = 12
        Me.btn_delete.Text = "delete"
        Me.btn_delete.UseVisualStyleBackColor = True
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(65, 153)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(73, 17)
        Me.Label5.TabIndex = 13
        Me.Label5.Text = "TELEPON"
        '
        'txt_telp
        '
        Me.txt_telp.Location = New System.Drawing.Point(180, 150)
        Me.txt_telp.Name = "txt_telp"
        Me.txt_telp.Size = New System.Drawing.Size(100, 20)
        Me.txt_telp.TabIndex = 14
        '
        'txt_alamat
        '
        Me.txt_alamat.Location = New System.Drawing.Point(462, 122)
        Me.txt_alamat.Multiline = True
        Me.txt_alamat.Name = "txt_alamat"
        Me.txt_alamat.Size = New System.Drawing.Size(237, 98)
        Me.txt_alamat.TabIndex = 16
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(347, 125)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(63, 17)
        Me.Label6.TabIndex = 15
        Me.Label6.Text = "ALAMAT"
        '
        'txt_jabatan
        '
        Me.txt_jabatan.Location = New System.Drawing.Point(180, 177)
        Me.txt_jabatan.Name = "txt_jabatan"
        Me.txt_jabatan.Size = New System.Drawing.Size(100, 20)
        Me.txt_jabatan.TabIndex = 18
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(65, 180)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(70, 17)
        Me.Label7.TabIndex = 17
        Me.Label7.Text = "JABATAN"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(65, 206)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(109, 17)
        Me.Label8.TabIndex = 19
        Me.Label8.Text = "JENIS KELAMIN"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(347, 70)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(108, 17)
        Me.Label9.TabIndex = 21
        Me.Label9.Text = "TEMPAT LAHIR"
        '
        'txt_tempat_lahir
        '
        Me.txt_tempat_lahir.Location = New System.Drawing.Point(462, 69)
        Me.txt_tempat_lahir.Name = "txt_tempat_lahir"
        Me.txt_tempat_lahir.Size = New System.Drawing.Size(128, 20)
        Me.txt_tempat_lahir.TabIndex = 24
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(347, 98)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(80, 17)
        Me.Label10.TabIndex = 23
        Me.Label10.Text = "TGL LAHIR"
        '
        'combo_jenis_kelamin
        '
        Me.combo_jenis_kelamin.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.combo_jenis_kelamin.FormattingEnabled = True
        Me.combo_jenis_kelamin.Location = New System.Drawing.Point(180, 203)
        Me.combo_jenis_kelamin.Name = "combo_jenis_kelamin"
        Me.combo_jenis_kelamin.Size = New System.Drawing.Size(100, 21)
        Me.combo_jenis_kelamin.TabIndex = 25
        '
        'datepicker_tgl_lahir
        '
        Me.datepicker_tgl_lahir.CustomFormat = "dd-MM-yyyy"
        Me.datepicker_tgl_lahir.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.datepicker_tgl_lahir.Location = New System.Drawing.Point(462, 96)
        Me.datepicker_tgl_lahir.Name = "datepicker_tgl_lahir"
        Me.datepicker_tgl_lahir.Size = New System.Drawing.Size(128, 20)
        Me.datepicker_tgl_lahir.TabIndex = 26
        '
        'btn_close
        '
        Me.btn_close.Image = Global.Presensi_Guru.My.Resources.Resources.close_logo
        Me.btn_close.Location = New System.Drawing.Point(740, 7)
        Me.btn_close.Name = "btn_close"
        Me.btn_close.Size = New System.Drawing.Size(39, 37)
        Me.btn_close.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.btn_close.TabIndex = 28
        Me.btn_close.TabStop = False
        '
        'btn_minimize
        '
        Me.btn_minimize.Image = CType(resources.GetObject("btn_minimize.Image"), System.Drawing.Image)
        Me.btn_minimize.Location = New System.Drawing.Point(697, 11)
        Me.btn_minimize.Name = "btn_minimize"
        Me.btn_minimize.Size = New System.Drawing.Size(36, 33)
        Me.btn_minimize.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.btn_minimize.TabIndex = 27
        Me.btn_minimize.TabStop = False
        '
        'btn_home
        '
        Me.btn_home.Image = Global.Presensi_Guru.My.Resources.Resources.home_logo
        Me.btn_home.Location = New System.Drawing.Point(646, 3)
        Me.btn_home.Name = "btn_home"
        Me.btn_home.Size = New System.Drawing.Size(49, 48)
        Me.btn_home.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.btn_home.TabIndex = 29
        Me.btn_home.TabStop = False
        '
        'Form_Guru_Master_Data
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Goldenrod
        Me.ClientSize = New System.Drawing.Size(800, 500)
        Me.Controls.Add(Me.btn_home)
        Me.Controls.Add(Me.btn_close)
        Me.Controls.Add(Me.btn_minimize)
        Me.Controls.Add(Me.datepicker_tgl_lahir)
        Me.Controls.Add(Me.combo_jenis_kelamin)
        Me.Controls.Add(Me.txt_tempat_lahir)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.txt_jabatan)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.txt_alamat)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.txt_telp)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.btn_delete)
        Me.Controls.Add(Me.btn_edit)
        Me.Controls.Add(Me.btn_add)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.txt_nama)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txt_nuptk)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txt_nip)
        Me.Controls.Add(Me.DataGridView1)
        Me.Controls.Add(Me.Label1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "Form_Guru_Master_Data"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Teacher_Master_Data_Page"
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btn_close, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btn_minimize, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btn_home, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label1 As Label
    Friend WithEvents DataGridView1 As DataGridView
    Friend WithEvents txt_nip As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents txt_nuptk As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents txt_nama As TextBox
    Friend WithEvents btn_add As Button
    Friend WithEvents btn_edit As Button
    Friend WithEvents btn_delete As Button
    Friend WithEvents Label5 As Label
    Friend WithEvents txt_telp As TextBox
    Friend WithEvents txt_alamat As TextBox
    Friend WithEvents Label6 As Label
    Friend WithEvents txt_jabatan As TextBox
    Friend WithEvents Label7 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents txt_tempat_lahir As TextBox
    Friend WithEvents Label10 As Label
    Friend WithEvents combo_jenis_kelamin As ComboBox
    Friend WithEvents datepicker_tgl_lahir As DateTimePicker
    Friend WithEvents btn_close As PictureBox
    Friend WithEvents btn_minimize As PictureBox
    Friend WithEvents btn_home As PictureBox
End Class
